import {Component, OnInit} from '@angular/core';
import {UsuarioService} from '../usuario.service';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  form: FormGroup;

  constructor(
    public usuarioService: UsuarioService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      nombre:  new FormControl('', [ Validators.required, Validators.pattern('^[a-zA-ZÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÑñüÜ \-\']+') ]),
      correo: new FormControl('', [ Validators.required, Validators.email ]),
      password:  new FormControl('', [ Validators.required, Validators.pattern('^[0-9a-zA-ZÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÑñüÜ*.-_ \-\']+') ]),
      tipo: new FormControl('', [ Validators.required])
    });
  }

  // tslint:disable-next-line:typedef
  get f(){
    return this.form.controls;
  }

  // tslint:disable-next-line:typedef
  submit(){
    this.usuarioService.crearUsuario(this.form.value).subscribe((resultado) => {
      alert(resultado.message);
      if (resultado.success) {
        this.router.navigateByUrl('usuario/index');
      }else{
        this.router.navigateByUrl('usuario/create');
      }
    });
  }

}

