import { Component, OnInit } from '@angular/core';

import { UsuarioService } from '../usuario.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  form: FormGroup;
  constructor(
    public usuarioService: UsuarioService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      correo: new FormControl('', [ Validators.required, Validators.email ]),
      password:  new FormControl('', [ Validators.required, Validators.pattern('^[0-9a-zA-ZÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÑñüÜ*.-_ \-\']+') ])
    });
    // sessionStorage.clear();
  }
  // tslint:disable-next-line:typedef
  get f(){
    return this.form.controls;
  }
  // tslint:disable-next-line:typedef
  submit(){
    this.usuarioService.login(this.form.value).subscribe((resultado) => {
      if (resultado.success) {
        sessionStorage.setItem('id', resultado.message.id);
        this.router.navigateByUrl(resultado.message.tipo + '/index');
      }else{
        alert(resultado.message);
        this.router.navigateByUrl('usuario/index');
      }
    });
  }

}
