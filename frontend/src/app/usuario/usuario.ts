export interface Usuario {
  success: any;
  message: any;
  id: number;
  nombre: string;
  correo: string;
  tipo: number;
}
