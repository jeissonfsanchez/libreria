export interface Empleado {
  success: any;
  message: any;
  id: number;
  nombre: string;
  correo: string;
  tipo: number;
}
