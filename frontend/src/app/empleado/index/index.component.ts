import { Component, OnInit } from '@angular/core';

import { EmpleadoService } from '../empleado.service';
import { Empleado } from '../empleado';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  empleados: Empleado[] = [];
  constructor(public empleadoService: EmpleadoService) { }

  ngOnInit(): void {
    this.empleadoService.listar().subscribe((data: Empleado[]) => {
      this.empleados = data;
    });
  }

  // tslint:disable-next-line:typedef
  borrarLibro(id){
    this.empleadoService.borrarLibro(id).subscribe(resultado => {
      this.empleados = this.empleados.filter(item => item.id !== id);
      alert(resultado.message);
    });
  }

  completarDonacion(libro_id){
    this.empleadoService.completarDonacion(libro_id).subscribe(resultado => {
      alert(resultado.message);
      window.location.reload();
    });
  }

}
