import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AutorRoutingModule } from './autor-routing.module';

import { IndexComponent } from './index/index.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PromocionComponent } from './promocion/promocion.component';
import { NuevasIdeasComponent } from './nuevas-ideas/nuevas-ideas.component';

@NgModule({
  declarations: [
    IndexComponent,
    CreateComponent,
    EditComponent,
    PromocionComponent,
    NuevasIdeasComponent
  ],
  imports: [
    CommonModule,
    AutorRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AutorModule { }

