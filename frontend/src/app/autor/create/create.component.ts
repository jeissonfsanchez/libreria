import { Component, OnInit } from '@angular/core';
import {AutorService} from '../autor.service';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  form: FormGroup;

  constructor(
    public autorService: AutorService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      nombre:  new FormControl('', [ Validators.required ]),
      descripcion: new FormControl('', [ Validators.required ]),
      precio: new FormControl('', [ Validators.required, Validators.pattern('^[0-9]*$') ])
    });
  }

  // tslint:disable-next-line:typedef
  get f(){
    return this.form.controls;
  }

  // tslint:disable-next-line:typedef
  submit(){
    this.autorService.crearLibro(this.form.value).subscribe((resultado) => {
      alert(resultado.message);
      if (resultado.success) {
        this.router.navigateByUrl('autor/index');
      }else{
        this.router.navigateByUrl('autor/create');
      }
    });
  }

}

