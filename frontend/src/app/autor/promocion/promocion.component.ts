import { Component, OnInit } from '@angular/core';
import {Autor} from '../autor';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AutorService} from '../autor.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-promocion',
  templateUrl: './promocion.component.html',
  styleUrls: ['./promocion.component.css']
})
export class PromocionComponent implements OnInit {

  fechaMin = new Date();
  autores: Autor[] = [];
  form: FormGroup;

  constructor(
    public autorService: AutorService,
    private route: ActivatedRoute,
    private router: Router
  ) { this.fechaMin.setDate(this.fechaMin.getDate() + 1); }

  ngOnInit(): void {
    this.form = new FormGroup({
      precio_descuento:  new FormControl('', [ Validators.required, Validators.pattern('^[0-9]*$') ]),
      fecha_hasta: new FormControl('', [ Validators.required ])
    });
  }

  // tslint:disable-next-line:typedef
  get f(){
    return this.form.controls;
  }

  // tslint:disable-next-line:typedef
  submit(){
    // tslint:disable-next-line:variable-name
    const id_promocion = this.route.snapshot.params.id_libro;
    const porcentaje = this.form.value.porcentaje;
    const fecha_hasta = this.form.value.fecha_hasta;
    const dataPromoLibro = {id_libro: id_promocion, porcentaje: porcentaje, fecha: fecha_hasta};
    this.autorService.crearPromo(dataPromoLibro).subscribe((resultado) => {
      alert(resultado.message);
      if (resultado.success) {
        this.router.navigateByUrl('autor/index');
      }else{
        this.router.navigateByUrl('autor/promocion');
      }
    });
  }
}
