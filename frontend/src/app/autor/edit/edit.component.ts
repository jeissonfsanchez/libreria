import { Component, OnInit } from '@angular/core';
import { AutorService } from '../autor.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { Autor } from '../autor';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  id: number;
  autores: Autor[] = [];
  form: FormGroup;

  constructor(
    public autorService: AutorService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id_libro;
    this.autorService.buscarLibro(this.id).subscribe((data: Autor[]) => {
      this.autores = data;
    });
    this.form = new FormGroup({
      nombre:  new FormControl('', [ Validators.required ]),
      descripcion: new FormControl('', [ Validators.required ]),
      precio: new FormControl('', [ Validators.required, Validators.pattern('^[0-9]*$') ])
    });

  }

  // tslint:disable-next-line:typedef
  get f(){
    return this.form.controls;
  }

  // tslint:disable-next-line:typedef
  submit(){
    this.autorService.actualizarLibro(this.id, this.form.value).subscribe((resultado) => {
      alert(resultado.message);
      this.router.navigateByUrl('autor/index');
    });
  }

}
