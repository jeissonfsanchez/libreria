import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AutorService } from '../autor.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Autor } from '../autor';

@Component({
  selector: 'app-nuevas-ideas',
  templateUrl: './nuevas-ideas.component.html',
  styleUrls: ['./nuevas-ideas.component.css']
})
export class NuevasIdeasComponent implements OnInit {

  autores: Autor[] = [];
  form: FormGroup;

  constructor(
    public autorService: AutorService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      fecha_maxima:  new FormControl('', [ Validators.required, Validators.pattern('^[0-9]*$') ]),
      valor_meta: new FormControl('', [ Validators.required, Validators.pattern('^[0-9]*$') ])
    });
  }

  // tslint:disable-next-line:typedef
  get f(){
    return this.form.controls;
  }

  // tslint:disable-next-line:typedef
  submit(){
    // tslint:disable-next-line:variable-name
    const id_nueva_idea = this.route.snapshot.params.id_libro;
    const valor_meta = this.form.value.valor_meta;
    const fecha_maxima = this.form.value.fecha_maxima;
    const dataNiLibro = {id_libro: id_nueva_idea, valor_meta: valor_meta, fecha_maxima: fecha_maxima};
    this.autorService.crearNuevaIdea(dataNiLibro).subscribe((resultado) => {
      alert(resultado.message);
      if (resultado.success) {
        this.router.navigateByUrl('autor/index');
      }else{
        this.router.navigateByUrl('autor/nuevas-ideas');
      }
    });
  }

}
