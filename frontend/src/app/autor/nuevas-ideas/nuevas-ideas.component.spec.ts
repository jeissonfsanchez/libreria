import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevasIdeasComponent } from './nuevas-ideas.component';

describe('NuevasIdeasComponent', () => {
  let component: NuevasIdeasComponent;
  let fixture: ComponentFixture<NuevasIdeasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NuevasIdeasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevasIdeasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
