import { Component, OnInit } from '@angular/core';

import { AutorService } from '../autor.service';
import { Autor } from '../autor';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  autores: Autor[] = [];
  constructor(public autorService: AutorService) { }

  ngOnInit(): void {
    this.autorService.listar().subscribe((data: Autor[]) => {
      this.autores = data;
    });
  }

  // tslint:disable-next-line:typedef
  borrarLibro(id){
    this.autorService.borrarLibro(id).subscribe(resultado => {
      alert(resultado.message);
      if (resultado.success) {
        this.autores = this.autores.filter(item => item.id !== id);
      }
    });
  }

  reportePDF(){
    const id = sessionStorage.id;
    this.autorService.reportePDF(id).subscribe(resultado => {
      const downloadURL = 'http://127.0.0.1:8000/storage/' + resultado.message;
      const link = document.createElement('a');
      link.href = downloadURL;
      link.target = '_blank';
      link.download = resultado.message;
      link.click();
    });
  }

}
