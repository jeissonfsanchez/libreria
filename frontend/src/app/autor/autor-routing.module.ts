import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndexComponent } from './index/index.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { NuevasIdeasComponent } from './nuevas-ideas/nuevas-ideas.component';
import { PromocionComponent } from './promocion/promocion.component';

const routes: Routes = [
  { path: 'autor', redirectTo: 'autor/index', pathMatch: 'full'},
  { path: 'autor/index', component: IndexComponent },
  { path: 'autor/create', component: CreateComponent },
  { path: 'autor/edit/:id_libro', component: EditComponent },
  { path: 'autor/promocion/:id_libro', component: PromocionComponent },
  { path: 'autor/nuevas-ideas/:id_libro', component: NuevasIdeasComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AutorRoutingModule { }
