import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import {  Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Autor } from './autor';

@Injectable({
  providedIn: 'root'
})
export class AutorService {

  private apiURL = 'http://localhost:8000/api/autor/';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private httpClient: HttpClient) { }

  listar(): Observable<Autor[]> {
    const id = sessionStorage.id;
    return this.httpClient.get<Autor[]>(this.apiURL + 'libro/' + id)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  crearLibro(usuario): Observable<Autor> {
    const data = {id: sessionStorage.id, nombre: usuario.nombre, descripcion: usuario.descripcion, precio: usuario.precio};
    return this.httpClient.post<Autor>(this.apiURL + 'libro/crear/' , JSON.stringify(data), this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  buscarLibro(id): Observable<Autor[]> {
    return this.httpClient.get<Autor[]>(this.apiURL + 'libro/consultar/' + id)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  actualizarLibro(id, usuario): Observable<Autor> {
    return this.httpClient.put<Autor>(this.apiURL + 'libro/actualizar/' + id, JSON.stringify(usuario), this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  // tslint:disable-next-line:typedef
  borrarLibro(id){
    return this.httpClient.delete<Autor>(this.apiURL + 'libro/eliminar/' + id, this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  crearNuevaIdea(dataNuevaIdea): Observable<Autor> {
    return this.httpClient.post<Autor>(this.apiURL + 'nueva_idea/crear/' , JSON.stringify(dataNuevaIdea), this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  crearPromo(dataNuevaIdea): Observable<Autor> {
    return this.httpClient.post<Autor>(this.apiURL + 'promocion/crear/' , JSON.stringify(dataNuevaIdea), this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  reportePDF(id){
    return this.httpClient.get<any>(this.apiURL + 'reporte/ventas/' + id, this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  // tslint:disable-next-line:typedef
  errorHandler(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Código de error: ${error.status}\nMensaje: ${error.message}`;
    }
    return throwError(errorMessage);
  }

}
