export interface Autor {
  success: any;
  message: any;
  id: number;
  nombre: string;
  tipo: number;
  descripcion: string;
  precio: number;
  valor_meta: number;
  fecha_maxima: number;
  fecha_hasta: any;
  porcentaje:number;
}
