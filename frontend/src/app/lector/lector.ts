export interface Lector {
  success: any;
  message: any;
  id: number;
  nombre: string;
  correo: string;
  tipo: number;
}

export interface Saldo{
  saldo: any;
  success: any;
  message: any;
}
