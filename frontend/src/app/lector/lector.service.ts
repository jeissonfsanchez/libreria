import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import {  Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import {Lector, Saldo} from './lector';

@Injectable({
  providedIn: 'root'
})
export class LectorService {

  private apiURL = 'http://localhost:8000/api/lector/';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private httpClient: HttpClient) { }

  listar(): Observable<Lector[]> {
    const id = sessionStorage.id;
    return this.httpClient.get<Lector[]>(this.apiURL + 'reserva/' + id)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  consultarSaldo(): Observable<Saldo> {
    const id = sessionStorage.id;
    return this.httpClient.get<Saldo>(this.apiURL + 'saldo/consultar/' + id)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  recargarSaldo(valor): Observable<Lector> {
    const info = {id: sessionStorage.id, recarga: valor.saldo, tipo: 'LECTOR'};
    return this.httpClient.post<Lector>(this.apiURL + 'saldo/recargar/', JSON.stringify(info), this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  // tslint:disable-next-line:typedef
  borrarLibro(id){
    return this.httpClient.delete<Lector>(this.apiURL + 'reserva/' + id, this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  searchPromotion(filtro): Observable<Lector[]> {
    const id = sessionStorage.id;
    return this.httpClient.post<Lector[]>(this.apiURL + 'promocion/' + id, JSON.stringify(filtro) , this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  // tslint:disable-next-line:typedef
  buyLibro(id, precio){
    const data = {id_libro: id, id_lector: sessionStorage.id, valor_compra: precio};
    return this.httpClient.post<Lector>(this.apiURL + 'reserva/', JSON.stringify(data) , this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  // tslint:disable-next-line:typedef
  donateIdea(id, donativo){
    const dato = JSON.stringify({donacion: donativo, id_lector: sessionStorage.id});
    return this.httpClient.put<any>(this.apiURL + 'nueva_idea/donar/' + id, dato, this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  searchNuevaIdea(): Observable<Lector[]> {
    return this.httpClient.get<Lector[]>(this.apiURL + 'nueva_idea/buscar/')
      .pipe(
        catchError(this.errorHandler)
      );
  }

  // tslint:disable-next-line:typedef
  errorHandler(error) {
    let errorMessage: string;
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Código de error: ${error.status}\nMensaje: ${error.message}`;
    }
    return throwError(errorMessage);
  }

}
