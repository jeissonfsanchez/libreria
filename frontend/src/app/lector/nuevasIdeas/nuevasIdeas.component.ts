import {Component, OnInit} from '@angular/core';
import {LectorService} from '../lector.service';
import {Lector} from '../lector';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './nuevasIdeas.component.html',
  styleUrls: ['./nuevasIdeas.component.css']
})
export class NuevasIdeasComponent implements OnInit {

  lectores: Lector[] = [];
  constructor(
    public lectorService: LectorService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.lectorService.searchNuevaIdea().subscribe((data: Lector[]) => {
      this.lectores = data;
    });
  }
  // tslint:disable-next-line:typedef
  donateIdea(id){
    const donativo = ((document.getElementById('donacion-' + id) as HTMLInputElement).value);
    // @ts-ignore
    if (donativo > 0) {
      this.lectorService.donateIdea(id, donativo).subscribe(resultado => {
        // this.lectores = this.lectores.filter(item => item.id !== id);
        alert(resultado.message);
        if (resultado.success) {
          window.location.reload();
        }
      });
    }else{
      alert('el donativo debe ser mayor a $0');
    }
  }
}

