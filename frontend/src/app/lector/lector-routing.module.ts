import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndexComponent } from './index/index.component';
import { PromotionComponent } from './promotion/promotion.component';
import { NuevasIdeasComponent } from './nuevasIdeas/nuevasIdeas.component';
import { SaldoComponent } from './saldo/saldo.component';

const routes: Routes = [
  { path: 'lector', redirectTo: 'lector/index', pathMatch: 'full'},
  { path: 'lector/index', component: IndexComponent },
  { path: 'lector/promotion', component: PromotionComponent },
  { path: 'lector/nuevasIdeas', component: NuevasIdeasComponent },
  { path: 'lector/saldo', component: SaldoComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LectorRoutingModule { }
