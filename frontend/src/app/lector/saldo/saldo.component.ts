import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LectorService} from '../lector.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-saldo',
  templateUrl: './saldo.component.html',
  styleUrls: ['./saldo.component.css']
})
export class SaldoComponent implements OnInit {

  form: FormGroup;

  constructor(
    public lectorService: LectorService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      saldo:  new FormControl('', [ Validators.required, Validators.pattern('^[0-9 \-\']+') ]),
    });
  }

  // tslint:disable-next-line:typedef
  get f(){
    return this.form.controls;
  }

  // tslint:disable-next-line:typedef
  submit(){
    if (this.form.value.saldo > 0) {
      this.lectorService.recargarSaldo(this.form.value).subscribe((resultado) => {
        alert(resultado.message);
        this.router.navigateByUrl('lector/index');
      });
    }else{
      alert('La recarga debe ser mayor a 0');
    }
  }

}
