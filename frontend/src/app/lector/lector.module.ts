import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LectorRoutingModule } from './lector-routing.module';

import { IndexComponent } from './index/index.component';
import { PromotionComponent } from './promotion/promotion.component';
import { NuevasIdeasComponent } from './nuevasIdeas/nuevasIdeas.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SaldoComponent } from './saldo/saldo.component';

@NgModule({
  declarations: [
    IndexComponent,
    PromotionComponent,
    NuevasIdeasComponent,
    SaldoComponent
  ],
  imports: [
    CommonModule,
    LectorRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class LectorModule { }

