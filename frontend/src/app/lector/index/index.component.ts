import { Component, OnInit } from '@angular/core';

import { LectorService } from '../lector.service';
import { Lector } from '../lector';
import { Saldo } from '../lector';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  saldo: Saldo;
  lectores: Lector[] = [];
  constructor(public lectorService: LectorService) { }

  ngOnInit(): void {
    this.lectorService.listar().subscribe((data: Lector[]) => {
      this.lectores = data;
    });
    this.lectorService.consultarSaldo().subscribe((data: Saldo) => {
      this.saldo = data.message;
    });
  }

  // tslint:disable-next-line:typedef variable-name
  borrarLibro(id, id_libro){
    // @ts-ignore
    this.lectorService.borrarLibro(id, id_libro).subscribe(resultado => {
      this.lectores = this.lectores.filter(item => item.id !== id);
      alert(resultado.message);
    });
  }

}
