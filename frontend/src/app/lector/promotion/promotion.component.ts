import {Component, OnInit} from '@angular/core';
import {LectorService} from '../lector.service';
import {Lector} from '../lector';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './promotion.component.html',
  styleUrls: ['./promotion.component.css']
})
export class PromotionComponent implements OnInit {

  lectores: Lector[] = [];
  filtro: string = '';
  constructor(
    public lectorService: LectorService,
    private router: Router
  ) { }

  ngOnInit(): void {
    const filtro = {filtro: ''};
    this.lectorService.searchPromotion(filtro).subscribe((data: Lector[]) => {
      this.lectores = data;
    });
  }
  // tslint:disable-next-line:typedef
  buyLibro(id, precio){
    this.lectorService.buyLibro(id, precio).subscribe(resultado => {
      this.lectores = this.lectores.filter(item => item.id !== id);
      alert(resultado.message);
      if (resultado.success) {
        this.router.navigateByUrl('lector/index');
      }
    });
  }
  // tslint:disable-next-line:typedef
  searchPromotion(filtro){
    const filter = {filtro: filtro};
    this.lectorService.searchPromotion(filter).subscribe((data: Lector[]) => {
      this.lectores = data;
    });
  }

}

