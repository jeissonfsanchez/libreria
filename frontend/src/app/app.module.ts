import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';

import { UsuarioModule } from './usuario/usuario.module';
import { AutorModule } from './autor/autor.module';
import { LectorModule } from './lector/lector.module';
import { EmpleadoModule } from './empleado/empleado.module';

import {UsuarioRoutingModule} from './usuario/usuario-routing.module';
import {LectorRoutingModule} from './lector/lector-routing.module';
import {AutorRoutingModule} from './autor/autor-routing.module';
import {EmpleadoRoutingModule} from './empleado/empleado-routing.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    AppRoutingModule,
    UsuarioModule,
    AutorModule,
    LectorModule,
    EmpleadoModule,
    HttpClientModule,
    UsuarioRoutingModule,
    LectorRoutingModule,
    AutorRoutingModule,
    EmpleadoRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
