<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<table style='padding:3%; background: #fff;width:100%; box-sizing: border-box !important;'>
    <tr>
        <td style='float:left; background: #fff; padding: 0px 5%; margin:auto'>
            <div>
                <p>
                    Hola <b>{{ $details['lector'] }}</b>
                </p>
                <p>
                    Se ha realizado la publicación del libro <b>"{{ $details['libro'] }}"</b> por parte del autor <b>{{ $details['autor'] }}</b>.
                </p>
                <p>Para reservarlo, puede ingresar a la plataforma con sus credenciales de acceso y dirigirse a la opción nuevas reservas. y reservarlo.</p>
                <p>
                    Gracias por preferirnos.
                </p>
            </div>
        </td>
    </tr>
</table>

<table style='padding:3%;background: #fff;width:100%;border-spacing:0px 0px; display:none'>
    <tr>
        <td style='width: 80%; padding: 2% 5%;margin:auto;'></td>
    </tr>
</table>

<table style='padding:3%;background: #fff;width:100%;border-spacing:0px 0px;'>
    <tr>
        <td style='width: 80%; padding: 2% 5%;margin:auto; background: #172184; color: #fff;'>
            <p style='font-size:12px;'>
                Atentamente,<br><br>Equipo de ventas
            </p>
        </td>
    </tr>
</table>


</body>
</html>


