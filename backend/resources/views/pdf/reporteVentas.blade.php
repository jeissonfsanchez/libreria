<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Reporte de ventas</title>
        <style>
            h1 {
                color:#c00;
                font-family:sans-serif;
                font-size:2em;
                margin-bottom:0;
                text-align: center;
            }
            table {
                width: 100%;
                font-family: sans-serif;
            }
            th, td {
                padding:.25em .5em;
                text-align:center;
            }
            td {
                background-color:#eee;
            }
            th {
                background-color:#009;
                color:#fff;
            }
        </style>
    </head>
    <body>
        <h1>Reporte de venta de libros</h1>
        <hr/>
        <table>
            <thead>
            <tr>
                <th class="header">Nombre</th>
                <th class="header">Cantidad</th>
                <th class="header">Fecha Publicación</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $dataVentas as $venta):
            <tr>
                <td>{{ $venta["nombre"]  }}</td>
                <td>{{ $venta["cantidad"] }}</td>
                <td>{{ $venta["publicacion"] }}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </body>
</html>

