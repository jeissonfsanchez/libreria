<?php

namespace App\Http\Controllers;

    use App\Models\Libreria\Reserva;
use App\Models\Libreria\Transaccion;
use App\Models\Usuario\Usuario;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LectorController extends Controller
{

    public function show($id)
    {
        $infoCompra = [];
        $reservaLector = Reserva::with(['dataLibro' => function ($query){
                $query->with('infoAutor')->with('infoLibreria');
            }])
            ->where(['id_lector'=>$id,'estado'=>'ACTIVO'])
            ->get();

        foreach ($reservaLector->toArray() as $infoReserva)
        {
            $info["id"] = $infoReserva["id"];
            $info["fecha_reserva"] = Carbon::parse($infoReserva["fecha_creacion"])->format('d-m-Y');
            $info["nombre_libro"] = $infoReserva["data_libro"]["nombre"];
            $info["autor_libro"] = $infoReserva["data_libro"]["info_autor"]["nombre"];
            $info["libreria"] = $infoReserva["data_libro"]["info_libreria"]["nombre"];
            $info["promocion"] = $infoReserva["data_libro"]["promocion"];
            $info["nueva_idea"] = $infoReserva["data_libro"]["nueva_idea"];
            array_push($infoCompra,$info);
        }

        return response()->json($infoCompra,200);
    }
}
