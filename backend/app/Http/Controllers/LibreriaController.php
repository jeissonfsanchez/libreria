<?php

namespace App\Http\Controllers;

use App\Models\Autor\Libro;
use App\Models\Libreria\Libreria;
use App\Models\Libreria\Transaccion;
use App\Models\Usuario\Usuario;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LibreriaController extends Controller
{
    public function index(){
       //Sin uso
    }

    public function show(Request $request)
    {
        $nuevasIdeas = Libro::where([
            'id_libreria'=>$request->id_libreria,
            'estado'=>'ACTIVO',
            'nueva_idea'=>'SI',
        ])->with('consultaNuevasIdeas')->get();
        $financiamiento = [];

        foreach ($nuevasIdeas as $key => $nuevaIdea){
            if ($nuevaIdea->consultaNuevasIdeas != null){
                $porcentajeRecaudoFaltante = $nuevaIdea->consultaNuevasIdeas->valor_meta * 100 / $nuevaIdea->consultaNuevasIdeas->valor_recaudo;

                $fechaApoyoLibreria = Carbon::parse($nuevaIdea->fecha_maxima)->subDays(30);
                $fechaMaxima = Carbon::now();
                $diasFaltantes = $fechaApoyoLibreria->diffInDays($fechaMaxima);
                if (($porcentajeRecaudoFaltante <= 40) && ($diasFaltantes < 30)) {
                    $financiamiento[$key]["id_libro"] = $nuevaIdea->id;
                    $financiamiento[$key]["nombre"] = $nuevaIdea->nombre;
                    $financiamiento[$key]["recaudo_faltante"] = $nuevaIdea->valor_meta - $nuevaIdea->valor_recaudo;
                    $financiamiento[$key]["porcentaje_faltante"] = round($porcentajeRecaudoFaltante,2);
                }
            }
        }
        return response()->json($financiamiento,200);
    }

    public function create(Request $request)
    {
        //Sin uso
    }

    public function update(Request $request)
    {
        //Sin uso
    }

    function destroy(Request $request)
    {
        //Sin uso
    }
}
