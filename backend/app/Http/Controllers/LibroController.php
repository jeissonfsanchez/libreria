<?php

namespace App\Http\Controllers;

use App\Mail\Correo;
use App\Models\Autor\Libro;
use App\Models\Usuario\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class LibroController extends Controller
{

    public function index(Request $request)
    {
        $libro = Libro::where('estado','ACTIVO')
            ->when(isset($request->nombre) && $request->nombre != "", function ($query) use($request){
                $query->where('nombre','like','%'.$request->nombre.'%');
            })
            ->when(isset($request->descripcion) && $request->descripcion != "", function ($query) use($request){
                $query->where('descripcion','like','%'.$request->descripcion.'%');
            })
            ->when(isset($request->autor) && $request->autor == "si", function ($query) use($request){
                $query->whereHas('infoAutor',function ($subquery) use ($request){
                    if(isset($request->nombre) && $request->nombre != "") {
                        $subquery->where('nombre_usuario', 'like', '%' . $request->nombreAutor . '%');
                    }
                });
            })
            ->with('infoAutor')
            ->orderBy('nombre','ASC')
            ->get();

        response()->json($libro, 200);
    }

    public function misLibros($id)
    {
        $infoLibros = [];
        $misLibros = Libro::where([
            'estado'=>'ACTIVO',
            'id_autor'=>$id
            ])
            ->with('consultaNuevasIdeas')
            ->get();

        foreach ($misLibros as $miLibro){
            $info['id'] = $miLibro->id;
            $info['nombre'] = $miLibro->nombre;
            $info['descripcion'] = $miLibro->descripcion;
            $info['precio'] = $miLibro->precio;
            $info['promocion'] = $miLibro->promocion;
            $info['nueva_idea'] = $miLibro->nueva_idea;
            $info['meta_cumplida'] = 'NO';
            if(!empty($miLibro->consultaNuevasIdeas)){
                $info['meta_cumplida'] = $miLibro->consultaNuevasIdeas->meta_cumplida;
                $info['porcentaje'] = ceil($miLibro->consultaNuevasIdeas->valor_recaudado * 100 / $miLibro->consultaNuevasIdeas->valor_meta);
            }
            array_push($infoLibros,$info);

        }
        return response()->json($infoLibros, 200);
    }

    public function consultarLibro($id)
    {
        $infoLibros = [];
        $misLibros = Libro::where([
                'estado' => 'ACTIVO',
                'id' => $id
            ])
            ->get();

        foreach ($misLibros as $miLibro){
            $info['id'] = $miLibro->id;
            $info['nombre'] = $miLibro->nombre;
            $info['descripcion'] = $miLibro->descripcion;
            $info['precio'] = $miLibro->precio;
            $info['editar_precio'] = !(($miLibro->promocion == 'SI') || ($miLibro->nueva_idea == 'SI'));
            array_push($infoLibros,$info);

        }
        return response()->json($infoLibros, 200);
    }

    public function crearLibro(Request $request)
    {
        Libro::create([
            "nombre" => $request->nombre,
            "id_autor" => $request->id,
            "descripcion" => $request->descripcion,
            "precio" => $request->precio
        ]);

        $nombreAutor = Usuario::where([
            'id' => $request->id,
            'estado' => 'ACTIVO'
        ])
        ->first();

        $notificarLectores = Usuario::where([
            'tipo' => 'LECTOR',
            'estado' => 'ACTIVO'
        ])
        ->get();

        foreach ($notificarLectores as $notificacion){
            $details = ['lector' => $notificacion->nombre, 'libro' => $request->nombre, 'autor' => $nombreAutor->nombre, 'vista' => 'promocion'];
            Mail::to($notificacion->correo)->send(new Correo($details));
        }

        return response()->json([
            'message' => "Se creó correctamente el libro.",
            'success' => true
        ], 200);
    }

    public function actualizarLibro(Request $request)
    {
        $arrayUpdate = [];
        if ($request->nombre && $request->nombre != ""){
            $arrayUpdate["nombre"] = $request->nombre;
        }
        if ($request->descripcion && $request->descripcion != ""){
            $arrayUpdate["descripcion"] = $request->descripcion;
        }
        if ($request->precio && $request->precio != ""){
            $arrayUpdate["precio"] = $request->precio;
        }

        Libro::where('id',$request->id)->update($arrayUpdate);

        return response()->json([
            'message' => "Se actualizó la información correctamente",
            'success' => true
        ], 200);
    }

    public function eliminarLibro($id)
    {
        Libro::where('id',$id)->update(["estado"=>"INACTIVO"]);
        return response()->json([
            'message' => "Se ha eliminado el libro.",
            'success' => true
        ], 200);

    }
}
