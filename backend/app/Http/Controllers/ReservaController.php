<?php

namespace App\Http\Controllers;

use App\Mail\Correo;
use App\Models\Autor\Libro;
use App\Models\Libreria\Reserva;
use App\Models\Libreria\Transaccion;
use App\Models\Usuario\Usuario;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ReservaController extends Controller
{
    public function misReservas($id)
    {
        $infoCompra = [];
        $reservaLector = Reserva::with(['dataLibro' => function ($query){
            $query->with('infoAutor')->with('infoLibreria');
            }])
            ->where(['id_lector'=>$id,'estado'=>'ACTIVO'])
            ->get();

        foreach ($reservaLector->toArray() as $infoReserva)
        {
            $info["id"] = $infoReserva["id"];
            $info["fecha_reserva"] = Carbon::parse($infoReserva["fecha_creacion"])->format('d-m-Y');
            $info["nombre_libro"] = $infoReserva["data_libro"]["nombre"];
            $info["autor_libro"] = $infoReserva["data_libro"]["info_autor"]["nombre"];
            $info["libreria"] = $infoReserva["data_libro"]["info_libreria"]["nombre"];
            $info["promocion"] = $infoReserva["data_libro"]["promocion"];
            $info["nueva_idea"] = $infoReserva["data_libro"]["nueva_idea"];
            array_push($infoCompra,$info);
        }

        return response()->json($infoCompra,200);
    }

    public function hacerReserva(Request $request)
    {
        $usuario = Usuario::where([
            "id"=>$request->id_lector,
            "estado"=>"ACTIVO"
        ])->first();

        if ($usuario->saldo >= $request->valor_compra) {

            Transaccion::create([
                "id_emisor" => $request->id_lector,
                "rol_emisor" => "LECTOR",
                "id_receptor" => $request->id_libro,
                "rol_receptor" => "LIBRO",
                "tipo_transaccion" => "COMPRA",
                "valor" => $request->valor_compra
            ]);

            Reserva::create([
                "id_libro" => $request->id_libro,
                "id_lector" => $request->id_lector,
                "valor_compra" => $request->valor_compra
            ]);

            $dataAutor = Libro::with('infoAutor','infoLibreria')->where('id', $request->id_libro)->first();

            $correo = $dataAutor->infoAutor->correo;
            $autor = $dataAutor->infoAutor->nombre;
            $libreria = $dataAutor->infoLibreria->nombre;
            $libro = $dataAutor->nombre;

            $details = ['autor' => $autor, 'libreria' => $libreria, 'libro' => $libro, 'vista' => 'reserva'];

            Mail::to($correo)->send(new Correo($details));

            $nuevoSaldo = $usuario->saldo - $request->valor_compra;

            if ($usuario->tipo == 'LECTOR') {
                $usuario->update(['saldo' => $nuevoSaldo]);
            }

            $mensaje = [
                'message' => "Se ha realizado la reserva correctamente.",
                'success' => true
            ];
        }
        else{
            $mensaje = [
                'message' => "No tiene el saldo suficiente para realizar la reserva.",
                'success' => true
            ];
        }

        return response()->json($mensaje,200);
    }

    public function destroy(Request $request)
    {
        Reserva::where('id',$request->id)->update(["estado"=>"INACTIVO"]);
        return response()->json([
            'message' => "Se ha eliminado la reserva del libro.",
            'success' => true
        ], 200);
    }
}
