<?php

namespace App\Http\Controllers;

use App\Models\Autor\Libro;
use App\Models\Libreria\Transaccion;
use App\Models\Usuario\Usuario;
use Illuminate\Http\Request;

class TransaccionController extends Controller
{
    public function index()
    {
        //Sin uso
    }

    public function create()
    {
        // PDF
    }

    public function store(Request $request)
    {
        //Sin uso
    }

    public function show(Request $request)
    {

        $dataLibros = Libro::where([
                'id_autor'=>$request->id_autor,
                'estado'=>'ACTIVO'
            ])
            ->with('ventasLibro')
            ->get();

        $reporteVentas = [];

        foreach ($dataLibros as $key => $dataLibro){
            if ($dataLibro != null){
                $reporteVentas[$key]["libro"] = $dataLibro->nombre;
                $reporteVentas[$key]["cantidad"] = $dataLibro->ventasLibro->count();
            }
        }
        return response()->json($reporteVentas,200);

    }


    public function update(Request $request, $id)
    {
        //Sin uso
    }

    public function destroy($id)
    {
        //Sin uso
    }
}
