<?php

namespace App\Http\Controllers;

use App\Mail\Correo;
use App\Models\Autor\Libro;
use App\Models\Autor\Promocion;
use App\Models\Usuario\Usuario;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PromocionController extends Controller
{
    public function promociones(Request $request,$id)
    {
        $dataLibros = Libro::with('consultaPromociones','infoAutor','infoLibreria')
            ->whereDoesntHave('misReservas',function($query) use($id){
                $query->where('id_lector',$id); // No muestra los libros que ya tiene reservados
            })
            ->when(isset($request->filtro) && $request->filtro != "", function ($query) use($request){
                $query->where('nombre','like','%'.$request->filtro.'%')
                    ->orWhere(function ($subquery) use ($request){
                        $subquery->whereHas('infoAutor', function ($subquery2) use ($request){
                            $subquery2->where('nombre', 'like', '%' . $request->filtro . '%');
                        });
                    });
            })
            ->where(["estado" => "ACTIVO", "nueva_idea" => "NO"])
            ->orderBy('nombre','ASC')
            ->get();

        $infoLibros = [];
        foreach ($dataLibros as $libro)
        {
            $info["id"] = $libro->id;
            $info["nombre_libro"] = $libro->nombre;
            $info["autor_libro"] = $libro->infoAutor->nombre;
            $info["libreria"] = $libro->infoLibreria->nombre;
            $info["precio"] = $libro->precio;
            $info["descuento"] = 0;
            $info["nuevo_precio"] = $libro->precio;
            $info["fecha_maxima"] = "Sin definir";
            if ($libro->consultaPromociones != null) {
                $info["descuento"] = $libro->consultaPromociones->porcentaje;
                $info["nuevo_precio"] = $libro->consultaPromociones->nuevo_precio;
                $info["fecha_maxima"] = Carbon::parse($libro->consultaPromociones->fecha)->format('d-m-Y');
            }
            array_push($infoLibros,$info);
        }

        return response()->json($infoLibros, 200);
    }

    public function crearPromocion(Request $request)
    {
        $libro = Libro::where('id',$request->id_libro)->with('infoAutor')->first();

        if ($libro != null) {
            $precioDescuento = ceil($libro->precio * $request->porcentaje / 100);
            Promocion::create([
                "id_libro" => $request->id_libro,
                "porcentaje" => $request->porcentaje,
                "nuevo_precio" => $precioDescuento,
                "fecha" => $request->fecha
            ]);

            $lectoresNotificacion = Usuario::where(["tipo" => "LECTOR", "estado" => "ACTIVO"])->get();

            foreach ($lectoresNotificacion as $infoLector) {
                $lector = $infoLector->nombre;
                $correo = $infoLector->correo;
                $autor = $infoLector->infoAutor->nombre;
                $libro = $infoLector->nombre;

                $details = ['lector' => $lector, 'libro' => $libro, 'autor' => $autor, 'vista' => 'promocion'];

                Mail::to($correo)->send(new Correo($details));
            }

            $mensaje = 'Promoción creada correctamente.';
            $bandera = true;
        }
        else{
            $mensaje = 'No se pudo realizar la promoción.';
            $bandera = false;
        }

        return response()->json([
            'message' => $mensaje,
            'success' => $bandera
        ], 200);
    }


    public function show(Request $request)
    {
        $promocion = Promocion::where([
            "id_libro"=>$request->id_libro,
            "estado"=>"ACTIVO"
        ])->first();

        return response()->json($promocion, 200);
    }

    public function update(Request $request)
    {
        $arrayUpdate = [];
        if (isset($request->porcentaje)){
            $libro = Libro::where('id',$request->id_libro)->first();
            $precioDescuento = $libro->precio * $request->porcentaje / 100;
            $arrayUpdate["porcentaje"] = $request->porcentaje;
            $arrayUpdate["nuevo_precio"] = $precioDescuento;
        }
        if (isset($request->fecha)){
            $arrayUpdate["fecha"] = $request->fecha;
        }

        Promocion::where("id_libro",$request->id_libro)->update($arrayUpdate);

        return response()->json([
            'message' => "Promoción actualizada correctamente.",
            'success' => true
        ], 200);
    }

    public function destroy(Request $request)
    {
        Promocion::where("id_libro",$request->id_libro)->update(["estado"=>"INACTIVO"]);

        return response()->json([
            'message' => "Promoción eliminada correctamente.",
            'success' => true
        ], 200);
    }
}
