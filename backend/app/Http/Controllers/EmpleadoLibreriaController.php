<?php

namespace App\Http\Controllers;

use App\Models\Autor\Libro;
use App\Models\Usuario\Usuario;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EmpleadoLibreriaController extends Controller
{

    public function consultarLibros($id){
        $libros = Libro::with('infoAutor')
            ->with('consultaNuevasIdeas')
            ->where(['id_libreria' => $id,'estado' => 'ACTIVO'])
            ->get();

        $dataLibro = [];

        foreach ($libros as $libro){
            $info['id'] = $libro->id;
            $info['nombre'] = $libro->nombre;
            $info['descripcion'] = $libro->descripcion;
            $info['precio'] = $libro->precio;
            $info['autor'] = $libro->infoAutor->nombre;
            $info['promocion'] = $libro->promocion;
            $info['nueva_idea'] = $libro->nueva_idea;
            if ($libro->consultaNuevasIdeas != null){
                $infoPorcentaje = ceil($libro->consultaNuevasIdeas->valor_recaudado * 100 / $libro->consultaNuevasIdeas->valor_meta);
                $info['porcentaje'] = $infoPorcentaje;
                $info['donacionLibreria'] = false;
                $fechaApoyoLibreria = Carbon::parse($libro->consultaNuevasIdeas->fecha_creacion);
                $fechaHoy = Carbon::now();
                $diasRecaudo = $fechaHoy->diffInDays($fechaApoyoLibreria);
                if ($infoPorcentaje >= 60 && $diasRecaudo < 30) {
                    $info['donacionLibreria'] = true;
                }
            }
            array_push($dataLibro,$info);
        }


        return response()->json($dataLibro,200);
    }

    function eliminarLibro($id)
    {
        Libro::where('id',$id)->update(["estado"=>"INACTIVO"]);
        return response()->json([
            'message' => "Libro eliminado correctamente.",
            'success' => true
        ],200);
    }
}
