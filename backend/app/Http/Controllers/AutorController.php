<?php

namespace App\Http\Controllers;

use App\Models\Autor\Libro;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class AutorController extends Controller
{
    public function reportePDF($id)
    {
        $infoVentas = Libro::withCount('ventasLibro')->where([
                'id_autor' => $id,
                'estado' => 'ACTIVO'
            ])
            ->get();
        $dataVentas = [];
        foreach ($infoVentas as $venta){
            $info['nombre'] = $venta->nombre;
            $info['publicacion'] = Carbon::parse($venta->fecha_creacion)->format('d-m-Y');
            $info['cantidad'] = $venta->ventas_libro_count;
            array_push($dataVentas,$info);
        }
        $nombrePDF = 'reporteVentas_'.uniqid().'.pdf';
        $data = compact('dataVentas');
        $pdf = PDF::loadView('pdf.reporteVentas', $data)->output();
        Storage::disk('public')->put($nombrePDF, $pdf);
        return response()->json([
            'message' => $nombrePDF,
            'success' => true
        ], 200);
    }


}
