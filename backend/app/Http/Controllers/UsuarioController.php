<?php

namespace App\Http\Controllers;

use App\Models\Libreria\Transaccion;
use App\Models\Usuario\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsuarioController extends Controller
{

    public function consultarSaldo($id){
        $saldo = Usuario::where([
                'id' => $id,
                'estado' => 'ACTIVO'
            ])
            ->select('saldo')
            ->first();

        return response()->json([
            'message' => $saldo->saldo,
            'success' => true
        ],200);

    }

    public function recargarSaldo(Request $request){
        $mensaje = "Recarga exitosa";
        $bandera = true;
        $usuario = Usuario::where([
            "id"=>$request->id,
            "estado"=>"ACTIVO"
        ])->first();

        if ($usuario != null){
            $nuevoSaldo = $usuario->saldo + $request->recarga;
            $usuario->update(['saldo'=>$nuevoSaldo]);

            Transaccion::create([
                "id_emisor" => $request->id,
                "rol_emisor" => $request->tipo,
                "id_receptor" => $request->id,
                "rol_receptor" => $request->tipo,
                "tipo_transaccion" => "RECARGA",
                "valor" => $request->recarga
            ]);
        }
        else{
            $mensaje = "No se pudo realizar la recarga";
            $bandera = false;
        }

        return response()->json([
            'message' => $mensaje,
            'success' => $bandera
        ],200);
    }

    public function verificarCredenciales(Request $request){
        $dataUsuario = Usuario::where(
            "correo",$request->correo
        )->first();

        if ($dataUsuario != null && Hash::check($request->password,$dataUsuario->pass_encrypt)){
            if ($dataUsuario->estado == "ACTIVO") {
                $mensaje = [
                    'message' => [
                        'tipo'=>strtolower($dataUsuario->tipo),
                        'id'=>$dataUsuario->id
                    ],
                    'success' => true
                ];
            }else{
                $mensaje = [
                    'message' => "Usuario eliminado de la plataforma.",
                    'success' => false
                ];
            }
        }else {
            $mensaje = [
                'message' => "Credenciales no válidas.",
                'success' => false
            ];
        }

        return response()->json($mensaje,200);
    }

    public function crearUsuario(Request $request){
        $mensaje = "Usuario creado correctamente.";
        $banMensaje = true;
        $usuario = Usuario::where('correo',$request->correo)->first();
        if (is_null($usuario)) {
            Usuario::create([
                "tipo" => $request->tipo,
                "nombre" => $request->nombre,
                "correo" => $request->correo,
                "pass_encrypt" => Hash::make($request->password),
                "pass_decrypt" => $request->password,
                "saldo" => 0
            ]);
        }else{
            $mensaje = "El correo ya existe.";
            $banMensaje = false;
        }

        return response()->json([
            'message' => $mensaje,
            'success' => $banMensaje
        ],200);
    }
}
