<?php

namespace App\Http\Controllers;

use App\Mail\Correo;
use App\Models\Autor\Libro;
use App\Models\Autor\NuevaIdea;
use App\Models\Libreria\Transaccion;
use App\Models\Usuario\Usuario;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class NuevaIdeaController extends Controller
{

    public function buscarNuevasIdeas()
    {
        $nI = NuevaIdea::where([
            'estado'=>'ACTIVO',
            'meta_cumplida'=>'NO'
            ])
            ->with(['infoLibro' => function($query){
                $query->with('infoLibreria')
                    ->with('infoAutor');
            }])
            ->get();

        $infoNuevaIdea = [];
        foreach ($nI->toArray() as $infoNI)
        {
            $info["id"] = $infoNI["id"];
            $info["nombre_libro"] = $infoNI["info_libro"]["nombre"];
            $info["autor_libro"] = $infoNI["info_libro"]["info_autor"]["nombre"];
            $info["libreria"] = $infoNI["info_libro"]["info_libreria"]["nombre"];
            $info["valor_recaudado"] = $infoNI["valor_recaudado"];
            $info["porcentaje"] = ceil($infoNI["valor_recaudado"] * 100 / $infoNI["valor_meta"]);
            $info["valor_meta"] = $infoNI["valor_meta"];
            $info["fecha_maxima"] = Carbon::parse($infoNI["fecha_maxima"])->format('d-m-Y');
            array_push($infoNuevaIdea,$info);
        }

        return response()->json($infoNuevaIdea,200);
    }

    public function crearDonacion(Request $request)
    {

        $libro = Libro::with('infoAutor')->where('id',$request->id_libro)->first();
        if ($libro != null) {
            $valorMeta = ($request->valor_meta > $libro->precio) ? $libro->precio : $request->valor_meta;

            $maxDias = ($request->fecha_maxima > 60) ? 60 : $request->fecha_maxima;
            $maxFecha = Carbon::parse(now())->addDays($maxDias)->format('Y-m-d');
            NuevaIdea::create([
                "id_libro" => $request->id_libro,
                "valor_recaudado" => 0,
                "fecha_maxima" => $maxFecha,
                "valor_meta" => $valorMeta
            ]);

            $lectoresNotificacion = Usuario::where(["tipo"=>"LECTOR","estado"=>"ACTIVO"])->get();

            $autor = $libro->infoAutor->nombre;
            $libro = $libro->nombre;

            $libro->update(['nueva_idea'=>'SI']);

            foreach ($lectoresNotificacion as $infoLector){
                $lector = $infoLector->nombre;
                $correo = $infoLector->correo;
                $details = ['lector' => $lector,'libro' => $libro,'autor'=>$autor,'vista'=>'nueva_idea'];

                Mail::to($correo)->send(new Correo($details));
            }

            $mensaje = [
                'message' => "Se ha creado la nueva recaudación para el libro.",
                'success' => true
            ];
        }
        else{
            $mensaje = [
                'message' => "No se pudo crear la nueva idea.",
                'success' => false
            ];
        }

        return response()->json($mensaje,200);

    }

    public function show(Request $request)
    {
        $nI = NuevaIdea::where([
            'id_libro',$request->id_libro,
            'estado'=>'ACTIVO'
        ])->get();
        return response()->json($nI,200);
    }

    public function realizarDonacion(Request $request, $id)
    {
        $mensaje = "";
        $bandera = false;
        $dataUsuario = Usuario::where('id',$request->id_lector)->first();

        if ((($dataUsuario->saldo - $request->donacion) >= 0 && $dataUsuario->saldo > 0) || ($dataUsuario->tipo == 'EMPLEADO')) {
            $nI = NuevaIdea::with('infoLibro')->where([
                "id"=>$id,
                "meta_cumplida"=>"NO",
                "estado"=>"ACTIVO"
            ])->first();
            if ($nI != null) {
                $mensajeExcedente = "";
                $banMeta = "SI";
                $valorFaltante = $nI->valor_meta - $nI->valor_recaudado;
                if($dataUsuario->tipo == 'LECTOR') {
                    $excedenteDonacion = $valorFaltante - $request->donacion;
                    if ($excedenteDonacion <= 0) {
                        $nuevoSaldoUsuario = $dataUsuario->saldo - $request->donacion + abs($excedenteDonacion);
                        $nuevoSaldoRecaudado = $nI->valor_meta;
                        $registroPrecioTransaccion = $request->donacion - $excedenteDonacion;
                        $mensajeExcedente = "La donación es superior al recaudo faltante para completar la meta. Se ha devuelto $" . abs($excedenteDonacion) . " a su cuenta.";
                    } else {
                        $banMeta = "NO";
                        $nuevoSaldoUsuario = $dataUsuario->saldo - $request->donacion;
                        $nuevoSaldoRecaudado = $nI->valor_recaudado + $request->donacion;
                        $registroPrecioTransaccion = $request->donacion;
                    }
                }else{
                    $nuevoSaldoUsuario = 0;
                    $nuevoSaldoRecaudado = $nI->valor_meta;
                    $registroPrecioTransaccion = $valorFaltante;
                }

                $rolEmisor = ($dataUsuario->tipo == "LECTOR") ?? "LIBRERIA";

                Transaccion::create([
                    "id_emisor"=>$dataUsuario->id,
                    "rol_emisor"=>$rolEmisor,
                    "id_receptor"=>$nI->id_libro,
                    "rol_receptor"=>"LIBRO",
                    "tipo_transaccion"=>"DONACION",
                    "valor"=>$registroPrecioTransaccion
                ]);

                $dataUsuario->update(["saldo"=>$nuevoSaldoUsuario]);
                $nI->update(["valor_recaudado"=>$nuevoSaldoRecaudado,"meta_cumplida"=>$banMeta]);

                $mensaje = 'Donación realizada correctamente. '.$mensajeExcedente;
                $bandera = true;
            }else{
                $mensaje = "No se pudo recibir el aporte de ayuda al libro.";
            }
        }else{
            $mensaje = "No tiene saldo suficiente para realizar la donación";
        }
        return response()->json([
            'message' => $mensaje,
            'success' => $bandera
        ],200);
    }

    public function destroy(Request $request)
    {
        NuevaIdea::where('id_libro', $request->id_libro,)->update(['estado' => 'INACTIVO']);

        return response()->json([
            'message' => "Se ha eliminado el libro del recaudo de donaciones.",
            'success' => true
        ], 200);
    }
}
