<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Correo extends Mailable
{
    use Queueable, SerializesModels;

    public $details;

    public function __construct($details)
    {
        $this->details = $details;
    }

    public function build()
    {

        if ($this->details["vista"] == 'reserva')
        {
        $correo = $this->subject("Reserva de libro")
            ->view('emails.mailCompra');
        }
        elseif ($this->details["vista"] == 'nueva_idea')
        {
            $correo = $this->subject("Donación")
                ->view('emails.mailNuevaIdea');
        }
        elseif ($this->details["vista"] == 'promocion')
        {
            $correo = $this->subject("Nuevo Libro")
                ->view('emails.mailNuevoLibro');
        }
        else{
            $correo = 'sin enviar notificación';
        }

        return $correo;
    }
}
