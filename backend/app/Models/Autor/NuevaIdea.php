<?php

namespace App\Models\Autor;

use Database\Factories\Autor\NuevaIdeaFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NuevaIdea extends Model
{
    use HasFactory;

    protected $model = NuevaIdea::class;

    protected $table = "nuevas_ideas";

    public $timestamps = false;

    protected $fillable = [
        "id_libro","valor_recaudado","fecha_maxima","valor_meta","meta_cumplida","estado"
    ];

    protected $guarded = [
        "id"
    ];

    protected $hidden = [
        "fecha_creacion","fecha_actualizacion"
    ];

    protected static function newFactory()
    {
        return NuevaIdeaFactory::new();
    }

    public function infoLibro(){
        return $this->hasOne(Libro::class,'id','id_libro')
            ->where('estado','ACTIVO');
    }
}
