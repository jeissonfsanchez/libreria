<?php

namespace App\Models\Autor;

use App\Models\Libreria\Libreria;
use App\Models\Libreria\Reserva;
use App\Models\Libreria\Transaccion;
use App\Models\Usuario\Usuario;
use Database\Factories\Autor\LibroFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{
    use HasFactory;

    protected $model = Libro::class;

    public $timestamps = false;

    protected $table = "libros";

    protected $fillable = [
        "nombre","id_autor","descripcion","precio","id_libreria","estado"
    ];

    protected $guarded = [
        "id","fecha_creacion"
    ];

    protected $hidden = [
        "fecha_actualizacion"
    ];

    protected static function newFactory()
    {
        return LibroFactory::new();
    }

    public function consultaPromociones(){
        return $this->hasOne(Promocion::class,'id_libro','id')
            ->where("estado","ACTIVO");
    }

    public function consultaNuevasIdeas(){
        return $this->hasOne(NuevaIdea::class,'id_libro','id')
            ->where("estado","ACTIVO")->whereIn("meta_cumplida",["SI","NO"]);
    }

    public function infoAutor(){
        return $this->hasOne(Usuario::class,'id','id_autor');
    }

    public function infoLibreria(){
        return $this->hasOne(Libreria::class,'id','id_libreria');
    }

    public function ventasLibro(){
        return $this->hasMany(Transaccion::class,'id_receptor','id')
            ->where(['rol_receptor' => 'LIBRO', 'tipo_transaccion' => 'COMPRA']);
    }

    public function misReservas(){
        return $this->hasOne(Reserva::class,'id_libro','id')
            ->where('estado','ACTIVO');
    }

}
