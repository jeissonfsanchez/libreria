<?php

namespace App\Models\Autor;

use App\Models\Libreria\Reserva;
use Database\Factories\Autor\PromocionFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Promocion extends Model
{
    use HasFactory;

    protected $model = Promocion::class;

    protected $table = "promociones";

    public $timestamps = false;

    protected $fillable = [
        "id_libro","porcentaje","nuevo_precio","fecha"
    ];

    protected $guarded = [
        "id"
    ];

    protected $hidden = [
        "fecha_creacion","fecha_actualizacion"
    ];

    protected static function newFactory()
    {
        return PromocionFactory::new();
    }

    public function infoLibro(){
        return $this->hasOne(Libro::class,'id','id_libro')
            ->where('estado','ACTIVO');
    }

    public function misReservas(){
        return $this->hasOne(Reserva::class,'id_libro','id_libro')
            ->where('estado','ACTIVO');
    }
}
