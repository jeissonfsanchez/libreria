<?php

namespace App\Models\Libreria;

use App\Models\Autor\Libro;
use Database\Factories\Libreria\ReservaFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    use HasFactory;

    protected $table = "reservas";

    protected $model = Reserva::class;

    public $timestamps = false;

    protected $fillable = [
        "id_libro","id_lector","valor_compra","estado"
    ];

    protected $guarded = [
        "id","fecha_creacion"
    ];

    protected $hidden = [
        "fecha_actualizacion"
    ];

    protected static function newFactory()
    {
        return ReservaFactory::new();
    }

    public function dataLibro(){
        return $this->hasOne(Libro::class,'id','id_libro');
    }
}
