<?php

namespace App\Models\Libreria;

use Database\Factories\Libreria\LibreriaFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Libreria extends Model
{
    use HasFactory;

    protected $model = Libreria::class;

    protected $table = "librerias";

    public $timestamps = false;

    protected $fillable = [
        "nombre","estado"
    ];

    protected $guarded = [
        "id"
    ];

    protected $hidden = [
        "fecha_creacion","fecha_actualizacion"
    ];

    protected static function newFactory()
    {
        return LibreriaFactory::new();
    }
}
