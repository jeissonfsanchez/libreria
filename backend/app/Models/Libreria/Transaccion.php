<?php

namespace App\Models\Libreria;

use App\Models\Autor\Libro;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaccion extends Model
{
    use HasFactory;

    protected $model = Transaccion::class;

    protected $table = "transacciones";

    public $timestamps = false;

    protected $fillable = [
        "id_emisor","rol_emisor","id_receptor","rol_receptor","tipo_transaccion","medio_transaccion","valor","estado"
    ];

    protected $guarded = [
        "id","fecha_creacion"
    ];

    protected $hidden = [
        "fecha_actualizacion"
    ];

    public function infoLibro(){
        return $this->hasOne(Libro::class,'id_receptor','id');
    }
}
