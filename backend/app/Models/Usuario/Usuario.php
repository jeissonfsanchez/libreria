<?php

namespace App\Models\Usuario;

use App\Models\Autor\Libro;
use App\Models\Libreria\Reserva;
use Database\Factories\Usuario\UsuarioFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    use HasFactory;

    protected $model = Usuario::class;

    protected $table = "usuarios";

    public $timestamps = false;

    protected $fillable = [
        "nombre","correo","pass_encrypt","pass_decrypt","saldo","estado"
    ];

    protected $guarded = [
        "id"
    ];

    protected $hidden = [
        "pass_encrypt","pass_decrypt","fecha_creacion","fecha_actualizacion","verificar_email"
    ];

    protected static function newFactory()
    {
        return UsuarioFactory::new();
    }

    public function misReservas(){
        return $this->hasMany(Reserva::class,'id_lector','id')
            ->where('estado','ACTIVO');
    }

    public function misLibros(){
        return $this->hasMany(Libro::class,'id_autor','id')
            ->where('estado','ACTIVO');
    }

}
