<?php

namespace App\Models\Usuario;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    use HasFactory;

    protected $model = Empleado::class;

    protected $table = "empleados";

    public $timestamps = false;

    protected $fillable = [
        "id_empleado","id_libreria"
    ];

    protected $guarded = [
        "id"
    ];
}
