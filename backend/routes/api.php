<?php

use App\Http\Controllers\AutorController;
use App\Http\Controllers\EmpleadoLibreriaController;
use App\Http\Controllers\LectorController;
use App\Http\Controllers\LibreriaController;
use App\Http\Controllers\LibroController;
use App\Http\Controllers\NuevaIdeaController;
use App\Http\Controllers\PromocionController;
use App\Http\Controllers\ReservaController;
use App\Http\Controllers\UsuarioController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('usuario')->group(function () {
    Route::post('/login/',[ UsuarioController::class, 'verificarCredenciales']);
    Route::post('/crearUsuario/',[ UsuarioController::class, 'crearUsuario']);
});

Route::prefix('lector')->group(function () {
    Route::get('/reserva/{id}',[ ReservaController::class, 'misReservas']);
    Route::post('/promocion/{id}',[ PromocionController::class, 'promociones']);
    Route::get('/nueva_idea/buscar/',[ NuevaIdeaController::class, 'buscarNuevasIdeas']);
    Route::get('/saldo/consultar/{id}',[ UsuarioController::class, 'consultarSaldo']);
    Route::post('/reserva/',[ ReservaController::class, 'hacerReserva']);
    Route::post('/saldo/recargar/', [UsuarioController::class,'recargarSaldo']);
    Route::put('/nueva_idea/donar/{id}',[ NuevaIdeaController::class, 'realizarDonacion']);
});

Route::prefix('autor')->group(function () {
    Route::get('/reporte/ventas/{id}',[ AutorController::class, 'reportePDF']);
    Route::get('/libro/{id}',[ LibroController::class, 'misLibros']);
    Route::get('/libro/consultar/{id}',[ LibroController::class, 'consultarLibro']);
    Route::put('/libro/actualizar/{id}',[ LibroController::class, 'actualizarLibro']);
    Route::delete('/libro/eliminar/{id}',[ LibroController::class, 'eliminarLibro']);
    Route::post('/libro/crear/',[ LibroController::class, 'crearLibro']);
    Route::post('/nueva_idea/crear/',[ NuevaIdeaController::class, 'crearDonacion']);
    Route::post('/promocion/crear/',[ PromocionController::class, 'crearPromocion']);
});

Route::prefix('empleado')->group(function () {
    Route::get('libros/consultar/{id}',[ EmpleadoLibreriaController::class, 'consultarLibros']);
    Route::post('/nueva_ideas/donar/{id}',[ NuevaIdeaController::class, 'realizarDonacion']);
    Route::delete('libros/borrar/{id}',[ EmpleadoLibreriaController::class, 'eliminarLibro']);
});
