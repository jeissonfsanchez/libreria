<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class MigracionTablas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* Migración de las tablas y sus Lláves Foráneas */

        Schema::create("usuarios", function ($tabla){
            $tabla->id();
            $tabla->enum("tipo",["LECTOR","AUTOR","EMPLEADO"])->default("LECTOR");
            $tabla->string("nombre",100);
            $tabla->string("correo",100)->unique();
            $tabla->string("pass_encrypt");
            $tabla->string("pass_decrypt",50);
            $tabla->bigInteger("saldo");
            $tabla->timestamp('verificar_email')->nullable();
            $tabla->enum("estado",["ACTIVO","INACTIVO"])->default("ACTIVO");
            $tabla->dateTime("fecha_registro")->useCurrent();
            $tabla->dateTime("fecha_actualizacion")->nullable()->useCurrentOnUpdate();
        });

        Schema::create('librerias', function (Blueprint $tabla) {
            $tabla->id();
            $tabla->string("nombre",100);
            $tabla->enum("estado",["ACTIVO","INACTIVO"])->default("ACTIVO");
            $tabla->dateTime("fecha_creacion")->useCurrent();
            $tabla->dateTime("fecha_actualizacion")->nullable()->useCurrentOnUpdate();
        });

        /*Schema::create('empleados', function (Blueprint $tabla) {
            $tabla->id();
            $tabla->unsignedBigInteger('id_empleado');
            $tabla->unsignedBigInteger('id_libreria');
            $tabla->dateTime("fecha_creacion")->useCurrent();
            $tabla->dateTime("fecha_actualizacion")->nullable()->useCurrentOnUpdate();
            // Llaves Foráneas
            $tabla->foreign("id_empleado")->references("id")->on("usuarios");
            $tabla->foreign("id_libreria")->references("id")->on("librerias");
        });*/

        Schema::create("libros", function (Blueprint $tabla) {
            $tabla->id();
            $tabla->string("nombre",50);
            $tabla->longText("descripcion");
            $tabla->integer("precio");
            $tabla->unsignedBigInteger("id_autor");
            $tabla->unsignedBigInteger("id_libreria")->default(1);
            $tabla->enum("promocion",["SI","NO"])->default("NO");
            $tabla->enum("nueva_idea",["SI","NO"])->default("NO");
            $tabla->enum("estado",["ACTIVO","INACTIVO"])->default("ACTIVO");
            $tabla->dateTime("fecha_creacion")->useCurrent();
            $tabla->dateTime("fecha_actualizacion")->nullable()->useCurrentOnUpdate();
            // Llaves Foráneas
            $tabla->foreign("id_autor")->references("id")->on("usuarios");
            $tabla->foreign("id_libreria")->references("id")->on("librerias");
        });

        Schema::create('promociones',function (Blueprint $tabla){
            $tabla->id();
            $tabla->unsignedBigInteger("id_libro");
            $tabla->integer("porcentaje");
            $tabla->integer("nuevo_precio");
            $tabla->date("fecha");
            $tabla->enum("estado",["ACTIVO","INACTIVO"])->default("ACTIVO");
            $tabla->dateTime("fecha_creacion")->useCurrent();
            $tabla->dateTime("fecha_actualizacion")->nullable()->useCurrentOnUpdate();
            //Llave Foránea
            $tabla->foreign("id_libro")->references("id")->on("libros");
        });

        Schema::create('nuevas_ideas',function (Blueprint $tabla){
            $tabla->id();
            $tabla->unsignedBigInteger("id_libro");
            $tabla->integer("valor_recaudado");
            $tabla->date("fecha_maxima");
            $tabla->integer("valor_meta");
            $tabla->enum("meta_cumplida",["SI","NO"])->default("NO");
            $tabla->enum("estado",["ACTIVO","INACTIVO"])->default("ACTIVO");
            $tabla->dateTime("fecha_creacion")->useCurrent();
            $tabla->dateTime("fecha_actualizacion")->nullable()->useCurrentOnUpdate();
            //Llave Foránea
            $tabla->foreign("id_libro")->references("id")->on("libros");
        });

        Schema::create('reservas',function (Blueprint $tabla){
            $tabla->id();
            $tabla->unsignedBigInteger("id_libro");
            $tabla->unsignedBigInteger("id_lector");
            $tabla->integer("valor_compra");
            $tabla->enum("estado",["ACTIVO","INACTIVO"])->default("ACTIVO");
            $tabla->dateTime("fecha_creacion")->useCurrent();
            $tabla->dateTime("fecha_actualizacion")->nullable()->useCurrentOnUpdate();
            //Llave Foránea
            $tabla->foreign("id_libro")->references("id")->on("libros");
            $tabla->foreign("id_lector")->references("id")->on("usuarios");
        });

        Schema::create("transacciones", function (Blueprint $tabla){
            $tabla->id();
            $tabla->integer("id_emisor");
            $tabla->enum("rol_emisor",["LECTOR","LIBRERIA"])->default("LECTOR");
            $tabla->integer("id_receptor");
            $tabla->enum("rol_receptor",["AUTOR","LECTOR","LIBRO"])->default("AUTOR");
            $tabla->enum("tipo_transaccion",["DONACION","COMPRA","RECARGA"])->default("COMPRA");
            $tabla->bigInteger("valor");
            $tabla->dateTime("fecha_creacion")->useCurrent();
            $tabla->dateTime("fecha_actualizacion")->nullable()->useCurrentOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /* Inversa de la tablas (de la más reciente a la primera) */

        Schema::dropIfExists("transacciones");

        Schema::table("nuevas_ideas", function (Blueprint $tabla){
            $tabla->dropForeign(["id_libro"]);
            $tabla->dropIfExists("nuevas_ideas");
        });

        Schema::table("reservas", function (Blueprint $tabla){
            $tabla->dropForeign(["id_lector"]);
            $tabla->dropForeign(["id_libro"]);
            $tabla->dropIfExists("promociones");
        });

        Schema::table("promociones", function (Blueprint $tabla){
            $tabla->dropForeign(["id_libro"]);
            $tabla->dropIfExists("promociones");
        });

        Schema::table("libros", function (Blueprint $tabla){
            $tabla->dropForeign(["id_autor"]);
            $tabla->dropForeign(["id_libreria"]);
            $tabla->dropIfExists("libros");
        });

       /* Schema::table("empleados", function (Blueprint $tabla){
            $tabla->dropForeign(["id_empleado"]);
            $tabla->dropForeign(["id_libreria"]);
            $tabla->dropIfExists("empleados");
        });*/

        Schema::dropIfExists("librerias");

        Schema::dropIfExists("usuarios");
    }
}
