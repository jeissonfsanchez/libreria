<?php

namespace Database\Factories\Libreria;

use App\Models\Libreria\Libreria;
use Illuminate\Database\Eloquent\Factories\Factory;

class LibreriaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Libreria::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "nombre" => $this->faker->company
        ];
    }
}
