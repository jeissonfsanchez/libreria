<?php

namespace Database\Factories\Libreria;

use App\Models\Autor\Libro;
use App\Models\Libreria\Reserva;
use App\Models\Usuario\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReservaFactory extends Factory
{

    protected $idLectorLista = [];

    protected $idLibroLista = [];

    protected $precioLibroLista = [];

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Reserva::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        if (empty($this->idLectorLista)) {
            $i = 0;
            $lector = Usuario::select('id')->where('tipo','LECTOR')->get();
            foreach ($lector as $a) {
                $this->idLectorLista[$i] = $a->id;
                $i++;
            }
        }
        $idLector = $this->faker->randomElement($this->idLectorLista);

        if (empty($this->idLibroLista)) {
            $i = 0;
            $libro = Libro::where('promocion','NO')->where('nueva_idea','NO')->get();
            foreach ($libro as $a) {
                $this->idLibroLista[$i] = $a->id;
                $this->precioLibroLista[$i] = $a->precio;
                $i++;
            }
        }
        $idLibro = $this->faker->randomElement($this->idLibroLista);

        $idConsulta = array_search($idLibro,$this->idLibroLista);

        $precioLibro = $this->precioLibroLista[$idConsulta];

        $this->idLibroLista = array_diff( $this->idLibroLista,[$idLibro]);

        $this->idLectorLista = array_diff( $this->idLectorLista,[$idLector]);

        return [
            "id_libro" => $idLibro,
            "id_lector" => $idLector,
            "valor_compra" => $precioLibro
        ];
    }
}
