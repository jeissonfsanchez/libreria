<?php

namespace Database\Factories\Usuario;

use App\Models\Libreria\Libreria;
use App\Models\Usuario\Empleado;
use App\Models\Usuario\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmpleadoFactory extends Factory
{
    protected $idListaEmpleado = [];

    protected $idListaLibreria = [];
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Empleado::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        if (empty($this->idListaEmpleado)) {
            $i = 0;
            $empleado = Usuario::where('tipo','EMPLEADO')->get();
            foreach ($empleado as $a) {
                $this->idListaEmpleado[$i] = $a->id;
                $i++;
            }
        }
        $idEmpleado = $this->faker->randomElement($this->idListaEmpleado);

        $this->idListaEmpleado = array_diff( $this->idListaEmpleado,[$idEmpleado]);

        if (empty($this->idListaLibreria)) {
            $i = 0;
            $libreria = Libreria::all();
            foreach ($libreria as $a) {
                $this->idListaLibreria[$i] = $a->id;
                $i++;
            }
        }
        $idLibreria = $this->faker->randomElement($this->idListaLibreria);

        return [
            "id_empleado"=>$idEmpleado,
            "id_libreria"=>$idLibreria
        ];
    }
}
