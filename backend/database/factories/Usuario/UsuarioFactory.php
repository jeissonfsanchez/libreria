<?php

namespace Database\Factories\Usuario;

use App\Models\Usuario\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

class UsuarioFactory extends Factory
{
    protected $contadorLector = 0;

    protected $contadorAutor = 0;

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Usuario::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(){

        if ($this->contadorLector < 2 && $this->contadorAutor == 0){
            $tipo = 'LECTOR';
            $this->contadorLector++;
        }
        elseif ($this->contadorLector == 2 && $this->contadorAutor < 2){
            $tipo = 'AUTOR';
            $this->contadorAutor++;
        }
        else{
            $tipo = 'EMPLEADO';
            $this->contadorLector = 0;
            $this->contadorAutor = 0;
        }

        $strPass = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789*.";
        $password = substr( str_shuffle($strPass), 0, 8 );
        return [
            "tipo" => $tipo,
            "nombre" => $this->faker->name,
            "correo" => $this->faker->safeEmail,
            "pass_encrypt" => Hash::make($password),
            "pass_decrypt" => $password,
            "saldo" => 0
        ];
    }
}
