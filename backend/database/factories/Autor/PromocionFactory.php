<?php

namespace Database\Factories\Autor;

use App\Models\Autor\Libro;
use App\Models\Autor\Promocion;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class PromocionFactory extends Factory
{
    protected $idLibroLista = [];

    protected $precioLibroLista = [];

    protected $fechaLibroLista = [];

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Promocion::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        if (empty($this->idLibroLista)) {
            $i = 0;
            $libro = Libro::all();
            foreach ($libro as $a) {
                $this->idLibroLista[$i] = $a->id;
                $this->precioLibroLista[$i] = $a->precio;
                $this->fechaLibroLista[$i] = $a->fecha_creacion;
                $i++;
            }
        }
        $idLibro = $this->faker->randomElement($this->idLibroLista);

        Libro::where('id',$idLibro)->update(['promocion'=> 'SI']);

        $idConsulta = array_search($idLibro,$this->idLibroLista);

        $descuento = $this->faker->numberBetween(10,90);
        $porcentajeDesc = 100-$descuento;
        $precioDesc = ceil(($this->precioLibroLista[$idConsulta]* $porcentajeDesc) / 100);
        $fechaDesc = $this->fechaLibroLista[$idConsulta];

        $this->idLibroLista = array_diff($this->idLibroLista,[$idLibro]);

        return [
            "id_libro" => $idLibro,
            "porcentaje" => $descuento,
            "nuevo_precio" => $precioDesc,
            "fecha" => Carbon::parse($fechaDesc)->addDays($this->faker->numberBetween(20,90))->format('Y-m-d')
        ];
    }
}
