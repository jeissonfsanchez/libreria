<?php

namespace Database\Factories\Autor;

use App\Models\Autor\Libro;
use App\Models\Libreria\Libreria;
use App\Models\Usuario\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;

class LibroFactory extends Factory
{
    protected $idAutorLista = [];
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Libro::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        if (empty($this->idAutorLista)) {
            $i = 0;
            $autor = Usuario::select('id')->where('tipo','AUTOR')->get();
            foreach ($autor as $a) {
                $this->idAutorLista[$i] = $a->id;
                $i++;
            }
        }
        $idAutor = $this->faker->randomElement($this->idAutorLista);

        $this->idAutorLista = array_diff( $this->idAutorLista,[$idAutor]);

        $libreria = Libreria::all();
        return [
            "nombre" => $this->faker->realText(50),
            "id_autor" => $idAutor,
            "descripcion" => $this->faker->realText,
            "precio" => $this->faker->numberBetween(20,2000),
            "id_libreria" => $this->faker->numberBetween(1,$libreria->count())
        ];
    }
}
