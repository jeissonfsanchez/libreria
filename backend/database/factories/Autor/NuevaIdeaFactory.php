<?php

namespace Database\Factories\Autor;

use App\Models\Autor\Libro;
use App\Models\Autor\NuevaIdea;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class NuevaIdeaFactory extends Factory
{

    protected $idLibroLista = [];

    protected $precioLibroLista = [];

    protected $fechaLibroLista = [];
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = NuevaIdea::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        if (empty($this->idLibroLista)) {
            $i = 0;
            $libro = Libro::where('promocion','<>','SI')->get();
            foreach ($libro as $a) {
                $this->idLibroLista[$i] = $a->id;
                $this->precioLibroLista[$i] = $a->precio;
                $this->fechaLibroLista[$i] = $a->fecha_creacion;
                $i++;
            }
        }
        $idLibro = $this->faker->randomElement($this->idLibroLista);

        Libro::where('id',$idLibro)->update(['nueva_idea'=>'SI']);

        $idConsulta = array_search($idLibro,$this->idLibroLista);

        $precioLibro = $this->precioLibroLista[$idConsulta];

        $fechaLibro = $this->fechaLibroLista[$idConsulta];


        $this->idLibroLista = array_diff($this->idLibroLista,[$idLibro]);
        return [
            "id_libro" => $idLibro,
            "valor_recaudado" => 0,
            "fecha_maxima" => Carbon::parse($fechaLibro)->addDays(60)->format('Y-m-d'),
            "valor_meta" => ($precioLibro * 0.5)
        ];
    }
}
