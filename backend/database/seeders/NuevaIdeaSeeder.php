<?php

namespace Database\Seeders;

use App\Models\Autor\NuevaIdea;
use Illuminate\Database\Seeder;

class NuevaIdeaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        NuevaIdea::factory(5)->create();
    }
}
