<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsuarioSeeder::class,
            LibreriaSeeder::class,
            //EmpleadoSeeder::class,
            LibroSeeder::class,
            PromocionSeeder::class,
            NuevaIdeaSeeder::class,
            ReservaSeeder::class
        ]);
    }
}
