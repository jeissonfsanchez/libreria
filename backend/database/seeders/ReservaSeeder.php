<?php

namespace Database\Seeders;

use App\Models\Libreria\Reserva;
use Illuminate\Database\Seeder;

class ReservaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Reserva::factory(5)->create();
    }
}
