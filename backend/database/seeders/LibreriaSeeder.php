<?php

namespace Database\Seeders;

use App\Models\Libreria\Libreria;
use Illuminate\Database\Seeder;

class LibreriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Libreria::factory(1)->create();
    }
}
