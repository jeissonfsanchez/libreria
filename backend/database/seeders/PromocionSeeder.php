<?php

namespace Database\Seeders;

use App\Models\Autor\Promocion;
use Illuminate\Database\Seeder;

class PromocionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Promocion::factory(5)->create();
    }
}
