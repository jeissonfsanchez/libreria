<?php

namespace Database\Seeders;

use App\Models\Autor\Libro;
use Illuminate\Database\Seeder;

class LibroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Libro::factory(15)->create();
    }
}
